<?php

namespace Types\Enum;

use MyCLabs\Enum\Enum;

class ArithmeticOperators extends Enum
{

    const ADD = 'add';
    const DIVIDE = 'divide';
    const SUBTRACT = 'subtract';
    const MULTIPLY = 'multiply';
    const MOD = 'mod';
    const INCREASE = 'increase';
    const DECREASE = 'decrease';
    const APPLY = 'apply';

}
