<?php

/**
 * Remove all spaces from a sting
 *
 * @param $str
 * @return string
 */
function removeSpaces($str) {
    return trim(preg_replace('/\s+/', ' ', $str));
}

/**
 * Return file content as an array
 *
 * @param $fileName
 * @return array
 */
function getFileContents($fileName) {
    $lines = [];
    $file = new \SplFileObject($fileName);
    foreach ($file as $line) {
        $lines[] = $line;
    }
    $file = null;
    return $lines;
}
