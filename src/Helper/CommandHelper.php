<?php

namespace Helper;

use Modal\Calculator\CalculatorFacade;
use Symfony\Component\Console\Helper\Helper as AbstractHelper;
use Symfony\Component\Console\Helper\HelperInterface;

class CommandHelper extends AbstractHelper implements HelperInterface
{

    /**
     * @return CalculatorFacade
     */
    public function getCalculatorFacade() {
        return new CalculatorFacade();
    }

    /**
     * @param $fileName
     * @return array
     */
    public function getFileContents($fileName) {
        return getFileContents($fileName);
    }

    public function getName() {
        return 'command_helper';
    }

}

