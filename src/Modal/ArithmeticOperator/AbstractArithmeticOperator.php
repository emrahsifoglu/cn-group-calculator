<?php

namespace Modal\ArithmeticOperator;

abstract class AbstractArithmeticOperator implements IArithmeticOperator
{

    protected $value = 0;
    protected $sign;

    /**
     * {@inheritDoc}
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * {@inheritDoc}
     */
    public function setValue($value) {
        $this->value = $value;
    }

    /**
     * {@inheritDoc}
     */
    public function getSign() {
        return $this->sign;
    }

    /**
     * {@inheritDoc}
     */
    public function setSign($sign) {
        $this->sign = $sign;
    }

    /**
     * {@inheritDoc}
     */
    public function getExplain($apply){
        if (!is_numeric($apply)) {
            $apply = "({$apply})";
        }
        $explain = $apply . ' ' . $this->getSign() . ' ' . $this->getValue();
        return $explain;
    }

}
