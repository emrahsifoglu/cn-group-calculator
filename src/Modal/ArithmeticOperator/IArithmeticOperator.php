<?php

namespace Modal\ArithmeticOperator;

interface IArithmeticOperator
{

    /**
     * Get value that operation will apply to.
     *
     * @return int
     */
    public function getValue();

    /**
     * Set value that operation will apply to.
     *
     * @param int $value
     */
    public function setValue($value);

    /**
     * Get operation's mathematical symbol.
     *
     * @return string
     */
    public function getSign();

    /**
     * Set operation's mathematical symbol.
     *
     * @param string $sign
     */
    public function setSign($sign);

    /**
     * Return only explain of the operation without performing.
     *
     * @param $apply
     * @return string
     */
    public function getExplain($apply);

    /**
     * Perform operation and return the result.
     *
     * @param $number
     * @return int
     */
    public function apply($number);

}
