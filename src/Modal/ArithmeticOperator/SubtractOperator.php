<?php

namespace Modal\ArithmeticOperator;

class SubtractOperator extends AbstractArithmeticOperator
{

    public function __construct() {
        $this->setSign('-');
    }

    /**
     * {@inheritDoc}
     */
    public function apply($number) {
        return $number - $this->getValue();
    }

}
