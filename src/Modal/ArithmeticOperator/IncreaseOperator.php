<?php

namespace Modal\ArithmeticOperator;

class IncreaseOperator extends AbstractArithmeticOperator
{

    public function __construct() {
        $this->setSign('+');
    }

    /**
     * Return only explain of the operation without performing.
     *
     * @param $apply
     * @return string
     */
    public function getExplain($apply){
        if (!is_numeric($apply)) {
            $apply = "({$apply})";
        }
        $explain = $apply . ' ' . $this->getSign() . ' ' . 1;
        return $explain;
    }

    /**
     * {@inheritDoc}
     */
    public function apply($number) {
        $number++;
        return $number;
    }

}
