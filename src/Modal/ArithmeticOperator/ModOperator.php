<?php

namespace Modal\ArithmeticOperator;

class ModOperator extends AbstractArithmeticOperator
{

    public function __construct() {
        $this->setSign('%');
    }

    /**
     * {@inheritDoc}
     */
    public function apply($number) {
        return $number % $this->getValue();
    }

}
