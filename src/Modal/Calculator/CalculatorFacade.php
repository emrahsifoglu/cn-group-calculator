<?php

namespace Modal\Calculator;

use Exception;
use Modal\ArithmeticOperator\IArithmeticOperator;
use Types\Enum\ArithmeticOperators;

class CalculatorFacade
{

    /**
     * Create operators from instructions given.
     *
     * Each line may contain string, which is operator, and value to perform an operation.
     * Except 'increase' and 'decrease', operator may not have value then it is gonna set to '0'.
     * If there is more than one 'apply' then first will be excepted.
     * However if there is no 'apply' then an error message will be thrown.
     *
     * @param array $instructions
     * @return array
     * @throws Exception
     */
    public function parseInstructions(array $instructions) {
        $arithmeticOperators = array_values(ArithmeticOperators::toArray());
        $pattern = '/^(\w+)(\s[0-9]+)?$/';
        $operators = [];
        $apply = null;

        foreach ($instructions as $instruction) {
            $subject = removeSpaces($instruction);
            $success = preg_match_all($pattern, $subject, $matches);

            if (!$success) {
                continue;
            }

            $operator = $matches[1][0];
            $value = removeSpaces($matches[2][0]);
            $value = is_numeric($value) ? (int)$value : 0;

            if ($operator == 'apply') {
                $apply = $apply ?: $value;
                continue;
            }

            if (!in_array($operator, $arithmeticOperators)) {
                continue;
            }

            $operator = ucfirst($operator);
            $Class = 'Modal\\ArithmeticOperator\\' . $operator . 'Operator';
            /** @var IArithmeticOperator $operator */
            $operator = new $Class();
            $operator->setValue($value);
            $operators[] = $operator;
        }

        if ($apply === null) {
            throw new Exception("'apply' can not be found");
        }

        return [
            'apply' => $apply,
            'operators' => $operators
        ];
    }

}