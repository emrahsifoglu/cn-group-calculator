<?php

namespace Modal\Calculator;

use Modal\ArithmeticOperator\IArithmeticOperator;

class Calculator
{

    private $arithmeticOperators;
    private $applyTo;

    public function __construct(

    ) {
        $this->arithmeticOperators = [];
    }

    /**
     * @return array
     */
    public function getArithmeticOperators() {
        return $this->arithmeticOperators;
    }

    public function addArithmeticOperator(IArithmeticOperator $arithmeticOperator) {
        $this->arithmeticOperators[] = $arithmeticOperator;
    }

    /**
     * @param array $arithmeticOperators
     */
    public function addArithmeticOperators(array $arithmeticOperators) {
        $this->arithmeticOperators = $arithmeticOperators;
    }

    public function removeArithmeticOperators() {
        $this->arithmeticOperators = [];
    }

    /**
     * @return int
     */
    public function getApplyTo() {
        return $this->applyTo;
    }

    /**
     * @param int $applyTo
     */
    public function setApplyTo($applyTo) {
        $this->applyTo = $applyTo;
    }

    /**
     * Return only explain of the whole operations without performing.
     *
     * @return string
     */
    public function getExplain() {
        $explain = $this->applyTo;
        /** @var IArithmeticOperator $operator */
        foreach ($this->arithmeticOperators as $index => $operator) {
            $explain = $operator->getExplain($explain);
        }
        return $explain;
    }

    /**
     * Perform all operations then return the result.
     *
     * @return int
     */
    public function calculate() {
        $apply = $this->applyTo;
        /** @var IArithmeticOperator $operator */
        foreach ($this->arithmeticOperators as $operator) {
            $apply = $operator->apply($apply);
        }
        return $apply;
    }

}