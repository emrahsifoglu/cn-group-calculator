<?php

namespace Command;

use Exception;
use Modal\Calculator\Calculator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\LockHandler;

class CalculateCommand extends Command
{

    protected function configure() {
        $this
            ->setName('cn:calculator')
            ->addArgument('instructions', InputArgument::REQUIRED, 'file path to instructions')
            ->setDescription('Calculate result from a set of instructions.');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $lock = new LockHandler('cn:calculator');
        if (!$lock->lock()) {
            $output->write('cn:calculator is already running in another process.');
            return 0;
        }

        $commandHelper = $this->getHelper('command_helper');

        try {
            $instructions = $commandHelper->getFileContents($input->getArgument('instructions'));
            $instructions = $commandHelper->getCalculatorFacade()->parseInstructions($instructions);

            $operators = $instructions['operators'];
            $apply = $instructions['apply'];

            $calculator = new Calculator();
            $calculator->setApplyTo($apply);

            if ($operators) {
                $calculator->addArithmeticOperators($operators);
            }

            $output->write($calculator->getExplain() . ' = ' . $calculator->calculate());
        } catch (Exception $e) {
            $output->write($e->getMessage());
        }

        $lock->release();
    }

}
