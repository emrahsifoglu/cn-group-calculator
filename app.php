<?php

require __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Command\CalculateCommand;

$application = new Application();
$application->getHelperSet()->set(new \Helper\CommandHelper());
$application->addCommands([new CalculateCommand()]);
$application->run();
