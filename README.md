# Coding Exercise

A command calculate a result from a set of instructions which comprise of a keyword and a number that are separated by a space per line.  

Instructions are loaded from file and results are output to the screen. Any number of Instructions can be specified. 

Instructions can be any binary operators of your choice (e.g., add, divide, subtract, multiply etc).  

The instructions will ignore mathematical precedence. The last instruction should be “apply” and a number (e.g., “apply 3”). 

The calculator is then initialised with that number and the previous instructions are applied to that number in order of appearance.

## Getting Started

### Technologies used

* [PhpStorm](https://www.jetbrains.com/phpstorm/) - The IDE
* [The Symfony Component](http://symfony.com/doc/2.8/components/console.html) - The Console Component

### Prerequisites

* Composer 
* Apache

You may use Nginx or another environment to run php.

### Installing

Install PHP packages

```
composer install
```

## Running

File path must be provided to the command.

Depending on your local setup, you may or may not use '.php' file ending.

```
php app.php cn:calculator instructions.txt
```

```
[Input from file]
add 2
multiply 3
increase
apply 4
```

```
[Output to console]
((4 + 2) * 3) + 1 = 19
```

## Testing

Under tests folder unit test can be found which covers 3 scenario.

First two cased are based on the actually content therefore instructions file must contain following content.

```
add 2
multiply 3
apply 4
```

In 3rd test case, the function - that gets file content - is mocked and instructions and results are provided. 

## Authors

* **Emrah Sifoğlu** - *Initial work* - [emrahsifoglu](https://github.com/emrahsifoglu)

## License

This project is a task thus source is kept in a private repo.
