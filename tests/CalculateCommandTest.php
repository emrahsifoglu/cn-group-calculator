<?php

use Command\CalculateCommand;
use Modal\Calculator\CalculatorFacade;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class CalculateCommandTest extends PHPUnit_Framework_TestCase
{

    /** @var Application */
    private $application;

    /** @var CalculateCommand */
    private $calculateCommand;

    protected function setUp() {
        $this->application = new Application();
        $this->calculateCommand = new CalculateCommand();
    }

    protected function tearDown() {
        Mockery::close();
        $this->application = null;
        $this->calculateCommand = null;
    }

	public function testContains() {
        $application = $this->application;
        $application->add($this->calculateCommand);

        $command = $application->find('cn:calculator');
        $command->getHelperSet()->set(new \Helper\CommandHelper(), 'command_helper');

        $commandTester = $this->getCommandTester($command);

        $output = $commandTester->getDisplay();
        $this->assertContains('18', $output);
    }

    public function testEquals() {
        $application = $this->application;
        $application->add($this->calculateCommand);

        $command = $application->find('cn:calculator');
        $command->getHelperSet()->set(new \Helper\CommandHelper(), 'command_helper');

        $commandTester = $this->getCommandTester($command);

        $output = $commandTester->getDisplay();
        $this->assertEquals('(4 + 2) * 3 = 18', $output);
    }

    /**
     * @dataProvider addDataProvider
     * @param $content
     * @param $result
     */
    public function testWithDataProvider($content, $result) {
        $application = $this->application;
        $application->add($this->calculateCommand);

        $command = $application->find('cn:calculator');

        $helper = $this->createMock(Helper\CommandHelper::class);
        $helper->expects($this->any())
            ->method('getFileContents')
            ->will($this->returnValue($content));

        $helper->expects($this->any())
            ->method('getCalculatorFacade')
            ->will($this->returnValue(new CalculatorFacade()));

        $command->getHelperSet()->set($helper, 'command_helper');

        $commandTester = $this->getCommandTester($command);

        $output = $commandTester->getDisplay();
        $this->assertEquals($result, $output);
    }

    /**
     * @param Command $command
     * @return CommandTester
     */
    private function getCommandTester(Command $command ) {
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'instructions' => 'instructions.txt'
        ]);
        return $commandTester;
    }

    public function addDataProvider() {
        return [
            [['add 2','multiply 3', 'apply 4'], '(4 + 2) * 3 = 18'],
            [['add 2','multiply 3', 'sub 2', 'apply 4'], '(4 + 2) * 3 = 18'], /* Result will be 18 because 'sub' is not defined*/
            [['add 2','multiply 3', 'subtract 2', 'apply 4'], '((4 + 2) * 3) - 2 = 16'],
            [['multiply 5', 'apply 9'], '9 * 5 = 45'],
            [['multiply 9', 'apply 5'], '5 * 9 = 45'],
            [['apply 1'], '1 = 1'],
        ];
    }

}
